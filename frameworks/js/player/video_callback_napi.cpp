/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "video_callback_napi.h"
#include "media_errors.h"
#include "media_log.h"

namespace {
constexpr OHOS::HiviewDFX::HiLogLabel LABEL = {LOG_CORE, LOG_DOMAIN, "VideoCallbackNapi"};
const std::string START_RENDER_FRAME_CALLBACK_NAME = "startRenderFrame";
const std::string VIDEO_SIZE_CHANGED_CALLBACK_NAME = "videoSizeChanged";
const std::string PLAYBACK_COMPLETED_CALLBACK_NAME = "playbackCompleted";
const std::string BITRATE_COLLECTED_CALLBACK_NAME = "availableBitrateCollected";
}

namespace OHOS {
namespace Media {
VideoCallbackNapi::VideoCallbackNapi(napi_env env)
    : PlayerCallbackNapi(env), env_(env)
{
    MEDIA_LOGD("0x%{public}06" PRIXPTR " Instances create", FAKE_POINTER(this));
}

VideoCallbackNapi::~VideoCallbackNapi()
{
    MEDIA_LOGD("0x%{public}06" PRIXPTR " Instances destroy", FAKE_POINTER(this));
}

void VideoCallbackNapi::QueueAsyncWork(VideoPlayerAsyncContext *context)
{
    std::lock_guard<std::mutex> lock(mutex_);
    switch (context->asyncWorkType) {
        case AsyncWorkType::ASYNC_WORK_PREPARE:
        case AsyncWorkType::ASYNC_WORK_PLAY:
        case AsyncWorkType::ASYNC_WORK_PAUSE:
        case AsyncWorkType::ASYNC_WORK_STOP:
        case AsyncWorkType::ASYNC_WORK_RESET:
            contextStateQue_.push(context);
            break;
        case AsyncWorkType::ASYNC_WORK_SEEK:
            contextSeekQue_.push(context);
            break;
        case AsyncWorkType::ASYNC_WORK_SPEED:
            contextSpeedQue_.push(context);
            break;
        case AsyncWorkType::ASYNC_WORK_VOLUME:
            contextVolumeQue_.push(context);
            break;
        case AsyncWorkType::ASYNC_WORK_BITRATE:
            contextBitRateQue_.push(context);
            break;
        default:
            MEDIA_LOGE("QueueAsyncWork type:%{public}d error", context->asyncWorkType);
            break;
    }
}

void VideoCallbackNapi::ClearAsyncWork()
{
    std::lock_guard<std::mutex> lock(mutex_);
    while (!contextStateQue_.empty()) {
        VideoPlayerAsyncContext *context = contextStateQue_.front();
        contextStateQue_.pop();
        delete context;
        context = nullptr;
    }
    while (!contextSeekQue_.empty()) {
        VideoPlayerAsyncContext *context = contextSeekQue_.front();
        contextSeekQue_.pop();
        delete context;
        context = nullptr;
    }
    while (!contextSpeedQue_.empty()) {
        VideoPlayerAsyncContext *context = contextSpeedQue_.front();
        contextSpeedQue_.pop();
        delete context;
        context = nullptr;
    }
    while (!contextVolumeQue_.empty()) {
        VideoPlayerAsyncContext *context = contextVolumeQue_.front();
        contextVolumeQue_.pop();
        delete context;
        context = nullptr;
    }
    while (!contextBitRateQue_.empty()) {
        VideoPlayerAsyncContext *context = contextBitRateQue_.front();
        contextBitRateQue_.pop();
        delete context;
        context = nullptr;
    }
}

void VideoCallbackNapi::OnInfo(PlayerOnInfoType type, int32_t extra, const Format &infoBody)
{
    std::lock_guard<std::mutex> lock(mutex_);
    MEDIA_LOGI("OnInfo is called, PlayerOnInfoType: %{public}d", type);
    switch (type) {
        case INFO_TYPE_MESSAGE:
            if (extra == PlayerMessageType::PLAYER_INFO_VIDEO_RENDERING_START) {
                VideoCallbackNapi::OnStartRenderFrameCb();
            }
            break;
        case INFO_TYPE_RESOLUTION_CHANGE:
            VideoCallbackNapi::OnVideoSizeChangedCb(infoBody);
            break;
        case INFO_TYPE_STATE_CHANGE:
            VideoCallbackNapi::OnStateChangeCb(static_cast<PlayerStates>(extra));
            break;
        case INFO_TYPE_SEEKDONE:
            VideoCallbackNapi::OnSeekDoneCb(extra);
            break;
        case INFO_TYPE_SPEEDDONE:
            VideoCallbackNapi::OnSpeedDoneCb(extra);
            break;
        case INFO_TYPE_BITRATEDONE:
            VideoCallbackNapi::OnBitRateDoneCb(extra);
            break;
        case INFO_TYPE_VOLUME_CHANGE:
            VideoCallbackNapi::OnVolumeDoneCb();
            break;
        case INFO_TYPE_BITRATE_COLLECT:
            VideoCallbackNapi::OnBitRateCollectedCb(infoBody);
            break;
        default:
            // video + audio common info
            PlayerCallbackNapi::OnInfo(type, extra, infoBody);
            break;
    }
    MEDIA_LOGD("send OnInfo callback success");
}

void VideoCallbackNapi::OnSeekDoneCb(int32_t position)
{
    if (contextSeekQue_.empty()) {
        MEDIA_LOGD("OnSeekDoneCb is called, But contextSeekQue_ is empty");
        return;
    }

    VideoPlayerAsyncContext *context = contextSeekQue_.front();
    CHECK_AND_RETURN_LOG(context != nullptr, "context is nullptr");
    contextSeekQue_.pop();
    context->JsResult = std::make_unique<MediaJsResultInt>(position);

    // Switch Napi threads
    VideoCallbackNapi::OnJsCallBack(context);
}

void VideoCallbackNapi::OnSpeedDoneCb(int32_t speedMode)
{
    if (contextSpeedQue_.empty()) {
        MEDIA_LOGD("OnSpeedDoneCb is called, But contextSpeedQue_ is empty");
        return;
    }

    VideoPlayerAsyncContext *context = contextSpeedQue_.front();
    CHECK_AND_RETURN_LOG(context != nullptr, "context is nullptr");
    contextSpeedQue_.pop();

    if (speedMode < SPEED_FORWARD_0_75_X || speedMode > SPEED_FORWARD_2_00_X) {
        MEDIA_LOGE("OnSpeedDoneCb mode:%{public}d error", speedMode);
    }

    context->JsResult = std::make_unique<MediaJsResultInt>(context->speedMode);
    // Switch Napi threads
    VideoCallbackNapi::OnJsCallBack(context);
}

void VideoCallbackNapi::OnBitRateDoneCb(int32_t bitRate)
{
    if (contextBitRateQue_.empty()) {
        MEDIA_LOGD("OnBitRateDoneCb is called, But contextBitRateQue_ is empty");
        return;
    }

    VideoPlayerAsyncContext *context = contextBitRateQue_.front();
    CHECK_AND_RETURN_LOG(context != nullptr, "context is nullptr");
    contextBitRateQue_.pop();

    context->JsResult = std::make_unique<MediaJsResultInt>(bitRate);
    // Switch Napi threads
    VideoCallbackNapi::OnJsCallBack(context);
}

void VideoCallbackNapi::OnVolumeDoneCb()
{
    if (contextVolumeQue_.empty()) {
        MEDIA_LOGD("OnVolumeDoneCb is called, But contextVolumeQue_ is empty");
        return;
    }

    VideoPlayerAsyncContext *context = contextVolumeQue_.front();
    CHECK_AND_RETURN_LOG(context != nullptr, "context is nullptr");
    contextVolumeQue_.pop();

    // Switch Napi threads
    VideoCallbackNapi::OnJsCallBack(context);
}

void VideoCallbackNapi::OnStartRenderFrameCb() const
{
    MEDIA_LOGD("OnStartRenderFrameCb is called");
    if (refMap_.find(START_RENDER_FRAME_CALLBACK_NAME) == refMap_.end()) {
        MEDIA_LOGW("can not find start render frame callback!");
        return;
    }
    PlayerJsCallback *cb = new(std::nothrow) PlayerJsCallback();
    CHECK_AND_RETURN_LOG(cb != nullptr, "No memory");
    cb->callback = refMap_.at(START_RENDER_FRAME_CALLBACK_NAME);
    cb->callbackName = START_RENDER_FRAME_CALLBACK_NAME;
    return PlayerCallbackNapi::OnJsCallBack(cb);
}

void VideoCallbackNapi::OnVideoSizeChangedCb(const Format &infoBody)
{
    (void)infoBody.GetIntValue(PlayerKeys::PLAYER_WIDTH, width_);
    (void)infoBody.GetIntValue(PlayerKeys::PLAYER_HEIGHT, height_);
    MEDIA_LOGD("OnVideoSizeChangedCb is called, width = %{public}d, height = %{public}d", width_, height_);
    if (refMap_.find(VIDEO_SIZE_CHANGED_CALLBACK_NAME) == refMap_.end()) {
        MEDIA_LOGW("can not find video size changed callback!");
        return;
    }
    PlayerJsCallback *cb = new(std::nothrow) PlayerJsCallback();
    CHECK_AND_RETURN_LOG(cb != nullptr, "No memory");
    cb->callback = refMap_.at(VIDEO_SIZE_CHANGED_CALLBACK_NAME);
    cb->callbackName = VIDEO_SIZE_CHANGED_CALLBACK_NAME;
    cb->valueVec.push_back(width_);
    cb->valueVec.push_back(height_);
    return PlayerCallbackNapi::OnJsCallBackIntVec(cb);
}

void VideoCallbackNapi::OnPlaybackCompleteCb() const
{
    MEDIA_LOGD("OnPlaybackCompleteCb is called");
    if (refMap_.find(PLAYBACK_COMPLETED_CALLBACK_NAME) == refMap_.end()) {
        MEDIA_LOGW("can not find completed callback!");
        return;
    }

    PlayerJsCallback *cb = new(std::nothrow) PlayerJsCallback();
    CHECK_AND_RETURN_LOG(cb != nullptr, "No memory");
    cb->callback = refMap_.at(PLAYBACK_COMPLETED_CALLBACK_NAME);
    cb->callbackName = PLAYBACK_COMPLETED_CALLBACK_NAME;
    return PlayerCallbackNapi::OnJsCallBack(cb);
}

PlayerStates VideoCallbackNapi::GetCurrentState() const
{
    return currentState_;
}

void VideoCallbackNapi::DequeueAsyncWork()
{
    if (contextStateQue_.empty()) {
        MEDIA_LOGW("OnStateChanged is called, But contextStateQue_ is empty");
        return;
    }

    VideoPlayerAsyncContext *context = contextStateQue_.front();
    CHECK_AND_RETURN_LOG(context != nullptr, "context is nullptr");

    bool needCb = false;
    switch (currentState_) {
        case PLAYER_PREPARED:
            needCb = context->asyncWorkType == AsyncWorkType::ASYNC_WORK_PREPARE ? true : false;
            break;
        case PLAYER_STARTED:
            needCb = context->asyncWorkType == AsyncWorkType::ASYNC_WORK_PLAY ? true : false;
            break;
        case PLAYER_PAUSED:
            needCb = context->asyncWorkType == AsyncWorkType::ASYNC_WORK_PAUSE ? true : false;
            break;
        case PLAYER_STOPPED:
            needCb = context->asyncWorkType == AsyncWorkType::ASYNC_WORK_STOP ? true : false;
            break;
        case PLAYER_IDLE:
            needCb = context->asyncWorkType == AsyncWorkType::ASYNC_WORK_RESET ? true : false;
            break;
        default:
            break;
    }

    if (needCb) {
        contextStateQue_.pop();
        // Switch Napi threads
        VideoCallbackNapi::OnJsCallBack(context);
    } else {
        MEDIA_LOGD("state:%{public}d is called, But context is empty", currentState_);
    }
}

void VideoCallbackNapi::OnStateChangeCb(PlayerStates state)
{
    MEDIA_LOGD("OnStateChanged is called, current state: %{public}d", state);
    currentState_ = state;

    switch (state) {
        case PLAYER_PREPARED:
        case PLAYER_STARTED:
        case PLAYER_PAUSED:
        case PLAYER_STOPPED:
        case PLAYER_IDLE:
            return DequeueAsyncWork();
        case PLAYER_PLAYBACK_COMPLETE:
            return OnPlaybackCompleteCb();
        default:
            break;
    }
}

void VideoCallbackNapi::OnBitRateCollectedCb(const Format &infoBody) const
{
    if (refMap_.find(BITRATE_COLLECTED_CALLBACK_NAME) == refMap_.end()) {
        MEDIA_LOGW("can not find bitrate collected callback!");
        return;
    }

    PlayerJsCallback *cb = new(std::nothrow) PlayerJsCallback();
    CHECK_AND_RETURN_LOG(cb != nullptr, "No memory");
    cb->callback = refMap_.at(BITRATE_COLLECTED_CALLBACK_NAME);
    cb->callbackName = BITRATE_COLLECTED_CALLBACK_NAME;

    uint8_t *addr = nullptr;
    size_t size  = 0;
    uint32_t bitrate = 0;
    if (infoBody.ContainKey(std::string(PlayerKeys::PLAYER_BITRATE))) {
        infoBody.GetBuffer(std::string(PlayerKeys::PLAYER_BITRATE), &addr, size);
        if (addr == nullptr) {
            delete cb;
            return;
        }

        MEDIA_LOGD("bitrate size = %{public}zu", size / sizeof(uint32_t));
        while (size > 0) {
            if ((size - sizeof(uint32_t)) < 0) {
                break;
            }

            bitrate = *(static_cast<uint32_t *>(static_cast<void *>(addr)));
            MEDIA_LOGD("bitrate = %{public}u", bitrate);
            addr += sizeof(uint32_t);
            size -= sizeof(uint32_t);
            cb->valueVec.push_back(static_cast<int32_t>(bitrate));
        }
    }

    return PlayerCallbackNapi::OnJsCallBackIntArray(cb);
}

void VideoCallbackNapi::UvWorkCallBack(uv_work_t *work, int status)
{
    napi_status nstatus = napi_generic_failure;
    switch (status) {
        case 0:
            nstatus = napi_ok;
            break;
        case UV_EINVAL:
            nstatus = napi_invalid_arg;
            break;
        case UV_ECANCELED:
            nstatus = napi_cancelled;
            break;
        default:
            nstatus = napi_generic_failure;
            break;
    }

    auto asyncContext = reinterpret_cast<MediaAsyncContext *>(work->data);
    if (asyncContext != nullptr) {
        MediaAsyncContext::CompleteCallback(asyncContext->env, nstatus, work->data);
    }
    delete work;
}

void VideoCallbackNapi::OnJsCallBack(VideoPlayerAsyncContext *context) const
{
    uv_loop_s *loop = nullptr;
    napi_get_uv_event_loop(env_, &loop);
    if (loop != nullptr) {
        uv_work_t *work = new(std::nothrow) uv_work_t;
        if (work != nullptr) {
            work->data = reinterpret_cast<void *>(context);
            int ret = uv_queue_work(loop, work, [] (uv_work_t *work) {}, VideoCallbackNapi::UvWorkCallBack);
            if (ret != 0) {
                MEDIA_LOGE("Failed to execute libuv work queue");
                delete context;
                delete work;
            }
        } else {
            delete context;
        }
    } else {
        delete context;
    }
}
} // namespace Media
} // namespace OHOS
